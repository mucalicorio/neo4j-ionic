import { Component } from '@angular/core';
import { v1 } from 'neo4j-driver';
import { Router } from '@angular/router';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  driver: any;
  session: any;
  user = {
    name: '',
    dateOfBirth: ''
  };

  btnEnabled = true;

  constructor(
    private router: Router,
    private platform: Platform
  ) {
    this.start();
  }

  start() {
    this.driver = v1.driver('http://localhost:7474/', v1.auth.basic('neo4j', 'querino'));
    this.session = this.driver.session();
  }

  save() {
    this.btnEnabled = false;
    const resultPromise = this.session.run(
      `MERGE (a:Person {
        name: $name,
        dateOfBirth: $dateOfBirth
      }) RETURN a`,
      this.user
    );

    resultPromise.then(result => {
      this.session.close();

      const singleRecord = result.records[0];
      const node = singleRecord.get(0);
      console.log(node.properties.name);

      this.user = {
        name: '',
        dateOfBirth: ''
      };
      this.btnEnabled = true;

      this.driver.close();
      this.router.navigateByUrl('/tabs/tab2');
    });
  }
}
