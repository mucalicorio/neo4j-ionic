import { Component } from '@angular/core';
import v1 from 'neo4j-driver';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  driver: any;
  session: any;
  users = [];
  loading = true;

  choosedPeople: any;

  constructor() { }

  ionViewWillEnter() {
    this.start();
    this.getItems();
  }

  start() {
    this.driver = v1.driver('http://localhost:7474/', v1.auth.basic('neo4j', 'querino'));
    this.session = this.driver.session();
  }

  getItems() {
    const resultPromise = this.session.run(
      `MATCH (n) RETURN n`
    );

    resultPromise.then((result: any) => {
      this.session.close();

      this.users = result.records;
      console.log('Users: ', this.users);

      this.driver.close();
      this.loading = false;
    });
  }

  fakeArrDataLoad(n: number): any[] {
    return Array(n);
  }

  choosePerson(person) {
    console.log('PERSON ID: ', person);

    if (this.choosedPeople) {
      this.start();
      const resultPromise = this.session.run(
        `MATCH (a), (b)
          WHERE (ID(a) = ${this.choosedPeople}) AND (ID(b) = ${person})
          MERGE (a)-[r:CYBER]->(b)
          RETURN a, b `
      );

      resultPromise.then((result: any) => {
        this.session.close();

        console.log('Result: ', result);

        this.driver.close();
        this.choosedPeople = null;
      });
    } else {
      this.choosedPeople = person;
      console.log('Choose Person: ', this.choosedPeople);
    }
  }

}
